docker-centos-7
===============

Custom CentOS 7 Dockerfile based on the official CentOS 7 Dockerfile.

* Removed `requiretty` from `/etc/sudoers`.
* Enabled systemd as described in [Running systemd within a Docker Container](http://developers.redhat.com/blog/2014/05/05/running-systemd-within-docker-container/).

Markus Juenemann <markus@juenemann.net>, 11-Nov-2016
